@extends('frontend.layouts.main')
@section('content')

  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-daygrid/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-timegrid/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-bootstrap/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/dist/css/adminlte.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

   
<div class="content-body">
    <h2> Timetable</h2>
	<div class="col-md-12">
		<div class="card card-info">
		<div class="card-header">
			<h3 class="card-title">Add Timetable</h3>
		</div>
		<div class="card-body">
			<form role="form">

			<div class="row">
			    <div class="col-sm-4">
			      	<div class="form-group">
			  	       <label>Date</label>
			    		<input type="date" class="form-control"  data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask>
					</div>
			    </div>
			       
			    <div class="col-sm-4">
			    	<div class="form-group">
					  	<label>Module</label>
					  	<select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
						    <option data-select2-id="3">COMP2020-Distributed Computing</option>
						    <option data-select2-id="39">COMP245-Operating Systems</option>
						    <option data-select2-id="40">DHI4875-Capstone Computing Project</option>
						    <option data-select2-id="41">DHH1658-Data Science</option>
						    <option data-select2-id="42">OS644-Interactive Media</option>
						    <option data-select2-id="43">TT11-Software Engineering</option>
						    <option data-select2-id="44">WTH444-Cyber Security</option>
					  	</select>
					</div>			     
			    </div>    

			  	<div class="col-sm-4">
			    	<div class="form-group">
					  	<label>Batch</label>
					  	<select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
						    <option data-select2-id="3">Y1S2G01</option>
						    <option data-select2-id="39">Y1S2G02</option>
						    <option data-select2-id="40">Y1S2G03</option>
						    <option data-select2-id="41">Y1S2G04</option>
						    <option data-select2-id="42">Y1S2G05</option>
						    <option data-select2-id="43">Y1S2G06</option>
						    <option data-select2-id="44">Y1S2G07</option>
					  	</select>
					</div>
			    </div>
			</div>

			<div class="row">
				<div class="col-sm-4">
			    	<div class="form-group">
					  	<label>Category</label>
					  	<select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
						    <option data-select2-id="3">Lecture</option>
						    <option data-select2-id="39">Tutorial</option>
						    <option data-select2-id="40">Lab Session</option>
						    <option data-select2-id="41">Other</option>
					  	</select>
					</div>
			    </div>

			 	<div class="col-sm-4">
			 		<div class="form-group">
	                    <label> Start Time:</label>

	                    <div class="input-group date" id="timepicker" data-target-input="nearest">
	                      	<input type="text" class="form-control datetimepicker-input" data-target="#timepicker"/>
	                      	<div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
	                        <div class="input-group-text"><i class="far fa-clock"></i></div>
	                    	</div>
	                    </div>
	                </div>
			    </div>    

			    <div class="col-sm-4">
			   	    <div class="form-group">
	                    <label>End Time:</label>
	                    <div class="input-group date" id="timepicker1" data-target-input="nearest">
	                      	<input type="text" class="form-control datetimepicker-input" data-target="#timepicker"/>
	                      	<div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
	                        	<div class="input-group-text"><i class="far fa-clock"></i></div>
	                    	</div>
	                    </div>
	                </div>
			   	</div>
			</div>

			<br>

			<div class="row">
				<div class="col-sm-2">
			    	<td><button type="button" class="btn btn-block btn-success btn-sm">Submit</button></td>
                </div>
           	</div>

		    </form>
	    </div>
	    </div>
	</div>
</div>


<style type="text/css">
h2 {
  color: #000000;
  text-align: center;
  text-transform: uppercase;
  text-shadow: 2px 2px #d8c2c2;
}
</style>

@endsection



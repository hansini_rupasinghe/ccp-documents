@extends('frontend.layouts.main')
@section('content')


<div class="wrapper"> 

  <h2>Timetable view</h2>
  <form action="{{ route('timetable.create')}}" method="GET">  
    @csrf
    <button class="btn btn-info" type="submit">Create New Record</button>  
  </form>
<br>
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="table-responsive">
          <table class="table table-striped">  
            <thead>  
              <tr>  
                <th>Date </th>  
                <th>Module </th>  
                <th>Category </th>  
                <th>Total Hours </th>
                <th>Actions </th>              
              </tr>  
            </thead>  
            <tbody>  
            @foreach($timetable as $tt)  
              <tr border="none"> 
                <td>{{$tt->ddate}}</td>  
                <td>{{ $tt->mod_code }} - {{ $tt->mod_name }}</td>  
                <td>{{$tt->category}}</td>  
                <td>{{$tt->tot_hours}}</td>

                <td>
                  <div class="btn-group"> 
                    <form action="{{ route('timetable.view', $tt->id)}}" method="GET">  
                      @csrf
                      <button class="btn btn-primary" type="submit" data-toggle="tooltip" title="View"><i class="fas fa-eye"></i></button>  
                    </form>
                    
                    <form action="{{ route('timetable.edit', [$tt->id])}}" method="GET">  
                      @csrf                     
                      <button class="btn btn-success" type="submit" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></button>  
                    </form>
                    
                    <form action="{{ route('timetable.delete', $tt->id)}}" method="post">  
                      @csrf
                      <button class="btn btn-danger" type="submit" data-toggle="tooltip" title="Delete"><i class="fas fa-trash-alt"></i></button>  
                    </form>
                  </div>
                </td>
              </tr>  
            @endforeach 

            </tbody>  
          </table>
          </div>
        </div>
      </div>
    </section>
</div>

<style type="text/css">
h2 {
  color: #000000;
  text-align: center;
  text-transform: uppercase;
  font-weight: bold;
  font-style: oblique;
  text-shadow: 2px 2px #d8c2c2;
}
</style>
 
@endsection  
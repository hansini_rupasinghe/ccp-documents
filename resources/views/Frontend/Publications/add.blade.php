@extends('frontend.layouts.main')
@section('content')

  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/dist/css/adminlte.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

   
  <div class="content-body">
    <h2> Publications </h2>
	<div class="col-md-12">
	            <div class="card card">
	              <div class="card-header" style="background-color: #66bb6a !important;">
	                <h3 class="card-title">Add Publication Details</h3>
	              </div>
	              <div class="card-body">
	                <form role="form" method="post" action="{{ route('publications.store') }}" >
	                	@csrf
	                  <div class="row">
	                    <div class="col-sm-5">
	                      <div class="form-group">
	                        <label>Publication Name / Title </label>
	                        <input name = "name" type="text" class="form-control" placeholder="Publication name here">
	                      </div>
	                    </div>
	                       
	                    <div class="col-sm-1"> 
	                    </div>    

	                  	<div class="col-sm-5">
	                      <div class="form-group">
	                        <label>Publication Description:</label>
	                        <textarea name = "pub_description" class="form-control" rows="3" placeholder="About the Publication"></textarea>
	                      </div>
	                    </div>
 					  </div>

 					  <div class="row">
	                    <div class="col-sm-5">
	                      <div class="form-group">
	                        <label>Publication Type</label>
                       		 <select name = "publication_type" class="form-control">
	                       	  <option>Select Publication Type</option>
	                          <option>Self Publication</option>
	                          <option>Publications with a Staff of Writers</option>
	                          <option>Magazines</option>
	                          <option>Scholarly Journals</option>
	                          <option>Books</option>
	                          <option>Newspaper Articles</option>
	                          <option>Conference Papers</option>
	                          <option>Theses</option>
	                          <option>Other</option>
	                        </select>
	                      </div>
	                    </div>
	                  

	                    <div class="col-sm-1"> 
	                    </div>  

	                    <div class="col-sm-5">
	                      <div class="form-group">
	                        <label>Publication Edition</label>
		                        <select name = "pub_version" class="form-control">
		                          <option>Select the Edition / Volume / Version </option>
		                          <option>First</option>
		                          <option>Second</option>
		                          <option>Third</option>
		                          <option>Fourth</option>
		                          <option>Fifth</option>
	                        </select>
	                      </div>
	                    </div>
	                   </div>

	                   <div class="row">
	                    <div class="col-sm-5">
	                   	    <div class="form-group">
	                  	        <label>Published Date</label>
								<input name = "pub_date" type="date" class="form-control"  data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask>
	                		</div>
	                   		</div>
	                   	
	                  
	                    <div class="col-sm-1"> 
                      </div> 
                      	</div>
                   
                      <br>
                      <div class="row">
                      <div class="col-sm-5">

                      <td>
                        <button type="submit" class="btn btn-block btn-success btn-sm">SAVE</button>
                      </td>
                      <br>
                     </div>
                 </div>
             </form>
         </div>
     </div>
  </div>
</div>


<style type="text/css">
h2 {
  color: #000000;
  text-align: center;
  text-transform: uppercase;
  text-shadow: 2px 2px #d8c2c2;
}
</style>

@endsection

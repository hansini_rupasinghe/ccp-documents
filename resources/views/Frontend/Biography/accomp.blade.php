@extends('frontend.layouts.main')
@section('content')

  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/dist/css/adminlte.min.css">
  

  <div class="container-fluid">
    <div class="card card-info">
      <div class="card-header">
        <h3 style="font-size:20px" class="card-title">Add Your Accomplishments</h3>
      </div>
      <div class="card-body">
         <form role="form" method="post" action="{{ route('accomplishments.store') }}">
        @csrf

        <label> Title </label>
        <input name="title" lass="form-control" type="text" placeholder="Ex: Won the best employee award">
        <br>

        <label> Description </label>
         <textarea name="desp" class="form-control" rows="3" placeholder="Enter the description"></textarea>
        <br>

      </div>

      <div align="right" class="card-footer">
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </div>    
  </div>


@endsection
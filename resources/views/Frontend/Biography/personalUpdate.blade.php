@extends('frontend.layouts.main')
@section('content')


  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  
  <div class="card card-warning">
    <div class="card-header">
      <h3 style="font-size:20px" class="card-title">Update Personal Information </h3>
    </div>
    
    <div class="card-body">
      <form role="form" method="post" action="{{ route('personal_info.update', $personal_info->id) }}">
        @csrf
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label>Full Name</label>
              <textarea name="full_name" class="form-control" rows="3" placeholder="Enter the full name" value="{{$personal_info->full_name}}"></textarea>
            </div>
          </div>
          
          <div class="col-sm-6">
            <label>Profile Image</label>
            <br>
            <input type="file" name="fileToUpload" id="fileToUpload">
            <!-- <input type="submit" value="Upload" name="submit"> -->
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label>Residential Address</label>
              <textarea name="resident_address" class="form-control" rows="3" placeholder="Residential Address" value="{{$personal_info->resident_address}}"></textarea>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="form-group">
              <label>Workplace Address</label>
              <textarea name="workplace_address" class="form-control" rows="3" placeholder="Workplace Address " value="{{$personal_info->workplace_address}}"></textarea>
            </div>
          </div>

        </div> 

        <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
              <label>NIC</label>
              <input name="nic" type="text" class="form-control" placeholder="Enter the NIC Number" value="{{$personal_info->nic}}">
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group">
              <label>Gender</label> &emsp; &emsp; 
              <div class="form-check" value="{{$personal_info->gender}}">              
                <input name="gender" type="radio" id="male" name="gender" value="M">
                <label for="male">Male</label> &emsp; &emsp; &emsp;  &emsp;  &emsp;  &emsp; 
                <input name="gender" type="radio" id="female" name="gender" value="F">
                <label for="female">Female</label><br> 
              </div>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group">
              <label>Email Address</label>
              <input name="email" type="text" class="form-control" value="{{$personal_info->email}}" >
            </div>
          </div>
        </div>
        

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">          
                <label>Date of Birth</label>
                <input name="dob" type="date" class="form-control"  data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" value="{{$personal_info->dob}}" data-mask>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="form-group">
              <label>Contact Number</label>
              <input name="contact" type="text" class="form-control" placeholder="Enter the contact number" value="{{$personal_info->contact}}">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-11"align="right" class="card-footer">
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>

          <br> <br> <br>
      </form>
    </div>
  </div>


<style type="text/css">
  
  img {
    padding-top: 10px;
    width: 200px;
    height: 200px;
  }

</style>

@endsection

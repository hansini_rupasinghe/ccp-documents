@extends('frontend.layouts.main')
@section('content')

  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/dist/css/adminlte.min.css">

  <div class="card card-info">
    <div class="card-header">
      <h3 style="font-size:20px" class="card-title">Attended Workshops and Conferences </h3>
    </div>
    <div class="card-body">
      <form role="form" method="post" action="{{ route('workshops.store') }}">
        @csrf

       <div class="form-group">
          <label> Workshop /Conference </label>
          <select name="workshop" class="form-control form-control-sm select2" style="width: 100%;">
            <option selected="selected">Please select</option>
            <option>Workshop</option>
            <option>Conference</option>
          </select>
        </div>
      <br>

      <label> Name of the Event </label>
      <input name="event_name" class="form-control form-control-sm" type="text" placeholder="Ex: Code Fest">
      <br>

      <label> Venue </label>
      <input name="venue" class="form-control form-control-sm" type="text" placeholder="Ex: Auditorium">
      <br>

      <label>Event Date</label>
      <input name="event_date" type="date" class="form-control"  data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask>
      <br>

    
      <div class="bootstrap-timepicker">
        <div class="form-group">
        <label>Event time:</label>
        <div class="input-group date" id="timepicker" data-target-input="nearest">
          <input name="event_time" type="text" class="form-control datetimepicker-input" data-target="#timepicker"/>
            <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
               <div class="input-group-text"><i class="far fa-clock"></i></div>
            </div>
        </div>
        </div>
      </div>

      <div align="right" class="card-footer">
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
  </div>
</div>

@endsection
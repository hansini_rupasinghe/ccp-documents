@extends('frontend.layouts.main')
@section('content')

  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/dist/css/adminlte.min.css">


  <section class="content">
    

      <div class="container-fluid">
<div class="card card-info">
              <div class="card-header">
                <h3 style="font-size:20px" class="card-title">Add Your Skills and Endorsements</h3>
              </div>
              <div class="card-body">
                <form role="form" method="post" action="{{ route('skills.store') }}">
                 @csrf

                <label> Add Skills </label>
                <input name="skills" class="form-control" type="text" placeholder="Ex: Won the best employee award">
                <br>
                <label> Description </label>
                 <textarea name="desp" class="form-control" rows="3" placeholder="Enter the description"></textarea>
                <br>

                 <label> Endorsements </label>
                 <textarea name="endorsements" class="form-control" rows="3" placeholder="Enter the description"></textarea>
                <br>

              </div>
              <div align="right" class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>

          
          </div>

        </section>

@endsection
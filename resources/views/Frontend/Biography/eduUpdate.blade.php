@extends('frontend.layouts.main')
@section('content')

  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/dist/css/adminlte.min.css">


  <div class="container-fluid">
    <div class="card card-info">
      <div class="card-header">
        <h3 style="font-size:20px" class="card-title">Update Your Educational Qualifications</h3>
      </div>
      <div class="card-body">
         <form role="form" method="post" action="{{ route('educational_qualification.update', $educational_qualification->id)  }}">
         @csrf

        <label> Institute </label>
        <input name="institute" class="form-control" type="text" placeholder="Ex: Sri Lanka Institute of Information Technology" value="{{$educational_qualification->institute}}">
        <br>
        <label> Degree </label>
        <input name="degree" class="form-control" type="text" placeholder="Ex: Bachelor Of Computing" value="{{$educational_qualification->degree}}" >
        <br>

        <label> Field of Study </label>
        <input name="field" class="form-control form-control-sm" type="text" placeholder="Ex: Information Technology" value="{{$educational_qualification->field}}" >
      
      <br>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Start Date</label>
              <select name="start_date" class="form-control select2" style="width: 100%;">
                <option selected="selected">Please select a year</option>
                <option>{{$educational_qualification->start_date}}</option>
                <option>1999</option>
                <option>2000</option>
                <option>2001</option>
                <option>2002</option>
                <option>2003</option>
                <option>2004</option>
                <option>2005</option>
                <option>2006</option>
                <option>2007</option>
                <option>2008</option>
                <option>2009</option>
                <option>2010</option>
                <option>2011</option>
                <option>2012</option>
                <option>2013</option>
                <option>2014</option>
                <option>2015</option>
                <option>2016</option>
                <option>2017</option>
                <option>2018</option>
                <option>2019</option>
                <option>2020</option>


              </select>
            </div>

          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>End Date </label>
              <select name="end_date" class="form-control select2" style="width: 100%;">
                <option selected="selected">Please select a year</option>
                 <option>{{$educational_qualification->end_date}}</option>
                <option>2030</option>
                <option>2029</option>
                <option>2028</option>
                <option>2027</option>
                <option>2026</option>
                <option>2025</option>
                <option>2024</option>
                <option>2023</option>
                <option>2022</option>
                <option>2021</option>
                <option>2020</option>
                <option>2019</option>
                <option>2018</option>
                <option>2017</option>
                <option>2016</option>
                <option>2015</option>
                <option>2014</option>
                <option>2013</option>
                <option>2012</option>
                <option>2011</option>
                <option>2010</option>
                <option>2009</option>
                <option>2008</option>
                <option>2007</option>
                <option>2006</option>
                <option>2005</option>
                <option>2004</option>
                <option>2003</option>
                <option>2002</option>
                <option>2001</option>
                <option>2000</option>
                <option>1999</option>

              </select>
            </div>

          </div>
        </div>
        
      </div>
                    
      <div align="right" class="card-footer">
          <button type="submit" class="btn btn-primary">Save</button>
      </div>
      </form>
    </div>
  </div>

@endsection
@extends('frontend.layouts.main')
@section('content')


  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <h2> User Biograghy </h2>
  <div class="row" style="background-image: url('/Images/bg.jpg'); height: 300px;">
    
    <div class="col-md-12">
      <div class="profile-sidebar" style="padding-left: 25px;">
        <img src="/Images/profile_img.jpg" alt="Paris">

        <div class="profile-user-title">
          <div style="font-size:20px; color: white" class="profile-user-name">
            <b> John Fedrick</b>
          </div>

          <div style="font-size:15px; color: white" class="profile-user-job">
           <b> Assistant Lecturer</b>
          </div>

          <div class="row">
            <div class="col-sm-10">
              <div class="profile-user-buttons">
                <button class="btn btn-success btn-sm">Change Photo</button>
              </div>
            </div>

            <div class="col-sm-1" style="float: right; padding-right: 80px;">
              <button style="width: 100px; float: right" type="button" class="btn btn-block btn-success">Preview</button>
            
            </div>

            <div class="col-sm-1" style="float: right;">
              <button style="width: 150px; float: right" type="button" class="btn btn-block btn-success">Save to PDF</button>
            </div>
          </div>      
        </div>            
      </div>
    </div>
  </div>
  
  <br><br>   
          
  <div class="card card-warning">
    <div class="card-header">
      <h3 style="font-size:20px" class="card-title">Personal Information </h3>
    </div>

    <div class="card-body">
      <form role="form">
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label>Full Name</label>
              <textarea class="form-control" rows="3" placeholder="Enter the full name"></textarea>
            </div>
          </div>
  
          <div class="col-sm-6">
            <div class="form-group">
              <label>Residential Address</label>
              <textarea class="form-control" rows="3" placeholder="Residential Address"></textarea>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label>NIC</label>
              <input type="text" class="form-control" placeholder="Enter the NIC Number" >
            </div>
          </div>

          <div class="col-sm-6">
            <div class="form-group">
              <label>Workplace Address</label>
              <textarea class="form-control" rows="3" placeholder="Workplace Address "></textarea>
            </div>
          </div>

        </div> 

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label>Gender</label> &emsp; &emsp; 
              <div class="form-check">              
                <input type="radio" id="male" name="gender" value="male">
                <label for="male">Male</label> &emsp; &emsp; &emsp;  &emsp;  &emsp;  &emsp; 
                <input type="radio" id="female" name="gender" value="female">
                <label for="female">Female</label><br> 
              </div>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="form-group">
              <label>Email Address</label>
              <input type="text" class="form-control" disabled>
            </div>
          </div>
        </div>
        

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">          
                <label>Date of Birth</label>
                <input type="date" class="form-control"  data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="form-group">
              <label>Contact Number</label>
              <input type="text" class="form-control" placeholder="Enter the contact number" >
            </div>
          </div>
        </div>

       

          <br> <br> <br>
      </form>
    </div>
  </div>

    <div class="row">
      <div class="col-sm-2">
        <button style="height:60px" type="button" class="btn btn-block btn-success">Projects</button>
      </div>

      <div class="col-sm-1"> 
      </div>

      <div class="col-sm-2">
        <button style="height:60px" type="button" class="btn btn-block btn-success">Researches</button>
      </div>

      <div class="col-sm-1"> 
      </div>

      <div class="col-sm-2">
        <button style="height:60px" type="button" class="btn btn-block btn-success">Publications</button>
      </div>

      <div class="col-sm-1"> 
      </div>

      <div class="col-sm-2">
        <button style="height:60px" type="button" class="btn btn-block btn-success">Workshops and Conferences</button>
      </div>


    </div>
                                
    <br> <br> 


  <div class="card card-warning">
    <div class="card-header">
      <h3 style="font-size:20px" class="card-title" style="relative">Working Experience </h3>
    </div>

    <!-- <div class="col-sm-12"> 
      <div class="input-group-append">
        <span style='position:absolute; right:16px;top:6px;' class="input-group-text"><a href="http://127.0.0.1:8000/experience"><i class="fas fa-plus" ></i> </a></span>
      </div> 
    </div>
    <br> -->

    <div class="card-body">
      <form role="form">
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <textarea style='white-space:pre; position: relative;' class="form-control" rows="6" placeholder="Designation
Organization
Time Period
Country"></textarea>

              <!-- <div class="input-group-append">
                <span style='position:absolute; right:16px;top:0px;' class="input-group-text"><i class="fas fa-pencil-alt" ></i></span>
              </div> --> 
            </div>
          </div>      
        </div> 
      </form>
    </div>
  </div>
           
  <div class="card card-warning">
    <div class="card-header">
      <h3 style="font-size:20px" class="card-title">Educational Qualifications </h3>
    </div>

    <!-- <div class="col-sm-12"> 
      <div class="input-group-append">
        <span style='position:absolute; right:16px;top:6px;' class="input-group-text"><a href="http://127.0.0.1:8000/education"><i class="fas fa-plus" ></i></a></span>
      </div> 
    </div>
    <br> -->

    <div class="card-body">
      <form role="form">
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <textarea style='white-space:pre; position: relative;' class="form-control" rows="6" placeholder="Institute
Degree
Field of Study
Time Period " ></textarea>

              <!-- <div class="input-group-append">
                <span style='position:absolute; right:16px;top:0px;' class="input-group-text"><i class="fas fa-pencil-alt" ></i></span>
              </div> -->
            </div>
          </div>      
        </div> 
      </form>      
    </div>
  </div>


  <div class="card card-warning">
    <div class="card-header">
      <h3 style="font-size:20px" class="card-title">Skills and Endorsements </h3>
    </div>

    <!-- <div class="col-sm-12"> 
      <div class="input-group-append">
        <span style='position:absolute; right:16px;top:6px;' class="input-group-text"> <a href="http://127.0.0.1:8000/skills"> <i class="fas fa-plus" ></i></a></span>
      </div> 
    </div>
    <br> -->
    
    <div class="card-body">
      <form role="form">
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">              
              <textarea style='position:relative;' class="form-control" rows="6" > </textarea>
              
              <!-- <div class="input-group-append">
                <span style='position:absolute; right:16px;top:0px;' class="input-group-text"><i class="fas fa-pencil-alt" ></i></span>
              </div> -->
            </div>
          </div>                  
        </div> 
      </form>
    </div>
  </div>

  <div class="card card-warning">
    <div class="card-header">
      <h3 style="font-size:20px" class="card-title">Accomplishments </h3>
    </div>

    <!-- <div class="col-sm-12"> 
      <div class="input-group-append">
        <span style='position:absolute; right:16px;top:6px;' class="input-group-text"> <a href="http://127.0.0.1:8000/accomplishments"> <i class="fas fa-plus" ></i></a></span>
      </div> 
    </div>
    <br> -->
    
    <div class="card-body">
      <form role="form">
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <textarea style='position:relative;' class="form-control" rows="6" > </textarea>

              <!-- <div class="input-group-append">
                <span style='position:absolute; right:16px;top:0px;' class="input-group-text"><i class="fas fa-pencil-alt" ></i></span>
              </div> -->
            </div>
          </div>      
        </div> 
      </form>
    </div>
  </div>


<style type="text/css">
  
  img {
    padding-top: 10px;
    width: 200px;
    height: 200px;
  }

</style>

@endsection

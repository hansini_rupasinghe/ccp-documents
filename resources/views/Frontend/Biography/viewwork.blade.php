@extends('frontend.layouts.main')
@section('content')

  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-daygrid/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-timegrid/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-bootstrap/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/dist/css/adminlte.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <section class="content">
    <h2>All Workshops and Conferences </h2>
    <div class="container-fluid">
      <div class="row">

        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="card-title">            
           <!--     <div class="input-group input-group-sm">
                  <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                  <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                      <i class="fas fa-search"></i>
                    </button>
                  </div>
                </div>
              </div>
                                
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Workshop/Conference</th>
                      <th>Name of the Event</th>
                      <th>Venue</th>
                      <th>Event Date</th>
                      <th>Event Time</th>
                      <th></th>

                    </tr>
                  </thead>

                  <tbody>
                    <tr>
                      <td>Conference</td>
                      <td>AGM</td>
                      <td>Main Auditorium</td>
                      <td>24.02.2020</td>
                      <td>06/23/2020 10:00 AM</td>
                      <td>
                        <div class="btn-group">
                          <button type="button" class="btn btn-success"><i class="fas fa-eye"></i></button>
                          <button type="button" class="btn btn-info"><i class="fas fa-edit"></i></button>
                          <button type="button" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                        </div>
                      </td>
                    </tr>

                    <tr>
                      <td>Workshop</td>
                      <td>Robotics</td>
                      <td>A506, Main Building</td>
                      <td>26.03.2020</td>
                      <td>03/21/2020 8:30 AM</td>
                      <td>
                        <div class="btn-group">
                          <button type="button" class="btn btn-success"><i class="fas fa-eye"></i></button>
                          <button type="button" class="btn btn-info"><i class="fas fa-edit"></i></button>
                          <button type="button" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                        </div>
                      </td>
                    </tr>
                            
                    <tr>
                      <td>Conference</td>
                      <td>Advancements in Computing</td>
                      <td>Engineering Auditorium</td>
                      <td>12.02.2019</td>
                      <td>09/12/2020 11:30 AM</td>
                      <td>
                        <div class="btn-group">
                          <button type="button" class="btn btn-success"><i class="fas fa-eye"></i></button>
                          <button type="button" class="btn btn-info"><i class="fas fa-edit"></i></button>
                          <button type="button" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                        </div>
                      </td>
                    </tr>

                    <tr>
                      <td>Workshop</td>
                      <td>Soft Skills</td>
                      <td>New Building</td>
                      <td>22.02.2018</td>
                      <td>07/02/2020 9:00 AM</td>
                      <td>
                        <div class="btn-group">
                          <button type="button" class="btn btn-success"><i class="fas fa-eye"></i></button>
                          <button type="button" class="btn btn-info"><i class="fas fa-edit"></i></button>
                          <button type="button" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
 -->

                  <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                      <button class="btn btn-navbar" type="submit">
                        <i class="fas fa-search"></i>
                      </button>
                    </div>
                  </div>
                </div>
                           
                                
                <div class="table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Workshop/Conference</th>
                        <th>Name of the Event</th>
                        <th>Venue</th>
                        <th>Event Date</th>
                        <th>Event Time</th>
                        <th></th>
                      </tr>
                    </thead>

                    <tbody>
                     @foreach($workshops as $wk)
                     <tr border="none">
                        <td>{{$wk->workshop}}</td>  
                        <td>{{$wk->event_name}}</td> 
                        <td>{{$wk->venue}}</td> 
                        <td>{{$wk->event_date}}</td> 
                        <td>{{$wk->event_time}}</td>
                        
                      <td>
                      <form action="{{ route('workshops.delete', $wk->id)}}" method="post">  
                         @csrf  
                         
                            <button class="btn btn-danger" type="submit">Delete</button>  
                      </form>
                    </td>

                    <td>  
                      <form action="{{ route('workshops.update', [$wk->id])}}" method="GET">  
                        @csrf  
                          <button class="btn btn-danger" type="submit">Edit</button>  
                      </form>
                    </td>
                    </tr>
                     @endforeach        
                    </tbody>

                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>


<style type="text/css">
h2 {
  color: #ffffff;
  background-color: #45c5d1;
  text-transform: uppercase;
  text-align: center;
  border-radius: 25px;
}
</style>

@endsection
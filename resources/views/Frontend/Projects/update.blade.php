@extends('frontend.layouts.main')
@section('content')

 <!-- Font Awesome -->
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- fullCalendar -->
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-daygrid/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-timegrid/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-bootstrap/main.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/almasaeed2010/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

   
  <div class="content-body">
    <h2> Projects </h2>
	<div class="col-md-15">
	            <!-- general form elements disabled -->
	            <div class="card card-indigo">
	              <div class="card-header">
	                <h3 class="card-title">Update Project Details</h3>
	              </div>
	              
	              <!-- /.card-header -->
	              <div class="card-body">
	                <form role="form" method="post" action="{{ route('projects.update',$projects) }}">
	                	@csrf
	                  <div class="row">
	                    <div class="col-sm-5">
	                      <div class="form-group">
	                        <label>Project Name </label>
	                        <input name = "topic" type="text" class="form-control" placeholder="Project name here" value="{{$projects->topic}}">
	                      </div>
	                    </div>
	                       
	                    <div class="col-sm-1"> 
	                    </div>    

	                  	<div class="col-sm-5">
	                      <div class="form-group">
	                        <label>Project Description:</label>
	                        <textarea name = "description" class="form-control" rows="3" placeholder="About the Project" value="{{$projects->description}}"> </textarea>
	                      </div>
	                    </div>
 					  </div>

 					  <div class="row">
	                    <div class="col-sm-5">
	                      <div class="form-group">
	                        <label>Project Status</label>
                       		 <select name = "project_type" class="form-control">
		                       	  <option>{{$projects->project_type}}</option>
		                          <option>Ongoing</option>
		                          <option>Completed</option>
	                        </select>
	                      </div>
	                    </div>

	                    <div class="col-sm-1"> 
	                    </div>  

	                    <div class="col-sm-5">
	                      <div class="form-group">
	                        <label>Module</label>
		                        <select name = "module" class="form-control">
		                          <option>{{$projects->module}}</option>
		                          <option value="1">CCP</option>
		                          <option value="1">HCI</option>
		                          <option value="1">ITP</option>
		                          <option value="1">IWT</option>
		                          <option value="1">IUP</option>
		                        </select>
	                      </div>
	                    </div>
	                </div>
	                     
	                 <div class="row">
	                    <div class="col-sm-5">
	                   	    <div class="form-group">
	                  	        <label>Started Date</label>
								<input name = "started_date" type="date" class="form-control"  data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask value="{{$projects->started_date}}"> 
	                		</div>
	                   		</div>
	               		 
	          			
	                    <div class="col-sm-1"> 
	                    </div>  


	                    <div class="col-sm-5">
	                      <div class="form-group">
	                        <label>Batch (Year / Semester) </label>
                        <select name = "batch" class="form-control">
                          <option>{{$projects->batch}}</option>
                          <option>Year 1 Semester 1</option>
                          <option>Year 1 Semester 2</option>
                          <option>Year 2 Semester 1</option>
                          <option>Year 2 Semester 2</option>
                          <option>Year 3 Semester 1</option>
                          <option>Year 3 Semester 2</option>
                          <option>Year 4 Semester 1</option>
                          <option>Year 4 Semester 2</option>
                        </select>
	                      </div>
	                    </div>
	                </div>

	               	 <div class="row">	
	                    <div class="col-sm-5">
	                   	    <div class="form-group">
	                  	        <label>Completed Date</label>
								<input name = "completed_date" type="date" class="form-control"  data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask value="{{$projects->completed_date}}">
	                		</div>
	                   		</div>
	               		 

	                    <div class="col-sm-1"> 
	                    </div>  

	               		 <div class="col-sm-5">
	                      <div class="form-group">
	                        <label>Estimated Time Period</label>
		                        <select name = "estimate_time" class="form-control">
			                        <option>{{$projects->estimate_time}}</option>
			                        <option>4 weeks</option>
			                        <option>6 weeks</option>
			                        <option>8 weeks</option>
			                        <option>10 weeks</option>
			                        <option>12 weeks</option>
			                        <option>20 weeks</option>
		                        </select>
	                      </div>
	                    </div>
	                 
	                  
	                    <div class="col-sm-1"> 
	                    </div> 
	                     
	                   
	               	</div> 
						<br>
						<div class="row">
						<div class="col-sm-5">
	                    <td>
	                      <button type="submit" class="btn btn-block btn-success btn-sm">SAVE</button>
	                    </td>
	                    <br>
	                   </div>
	               </div>
	           </form>
	       </div>
	   </div>
	</div>
</div>

<style type="text/css">
h2 {
  color: #000000;
  text-align: center;
  text-transform: uppercase;
  text-shadow: 2px 2px #d8c2c2;
}
</style>

@endsection
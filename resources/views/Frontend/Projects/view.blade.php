@extends('frontend.layouts.main')
@section('content')

  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-daygrid/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-timegrid/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-bootstrap/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/dist/css/adminlte.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <section class="content">
    <h2>All Projects</h2>
    <div class="container-fluid">
      <div class="row">

        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">

              <div class="card-title">             
                <div class="input-group input-group-sm">
                  <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                  <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                      <i class="fas fa-search"></i>
                    </button>
                  </div>
                </div>
              </div>
                                
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Project Name</th>
                      <th>Module</th>
                      <th>Batch</th>
                      <th>Started Date</th>
                      <th>Completed Date</th>
                      <th></th>

                    </tr>
                  </thead>

                  <tbody>
                    @foreach($projects as $tt)
                    <tr border="none">
                      <td>{{$tt->topic}}</td>
                      <td>{{$tt->module}}</td>
                      <td>{{$tt->batch}}</td>  
                      <td>{{$tt->started_date}}</td>  
                      <td>{{$tt->completed_date}}</td> 
                      
                    <td>
                      <form action="{{ route('projects.delete', $tt->id)}}" method="post">  
                         @csrf  
                          <!-- // @method('DELETE')  -->
                            <button class="btn btn-danger" type="submit">Delete</button>  
                      </form>
                    </td>

                    <td>  
                      <form action="{{ route('projects.update', [$tt->id])}}" method="GET">  
                        @csrf  
                          <button class="btn btn-danger" type="submit">Edit</button>  
                      </form>
                    </td>
                    </tr>
                  @endforeach
        
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


<style type="text/css">
h2 {
  color: #ffffff;
  background-color: #6610f2;
  text-transform: uppercase;
  text-align: center;
  border-radius: 25px;
}
</style>

@endsection
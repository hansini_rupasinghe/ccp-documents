@extends('frontend.layouts.main')
@section('content')

  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-daygrid/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-timegrid/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-bootstrap/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/dist/css/adminlte.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

   
  <div class="content-body">
    <h2> Researches </h2>
  <div class="col-md-15">
              <div class="card card">
                <div class="card-header" style="background-color: #ffb342 !important;">
                  <h3 class="card-title">Update Research Details</h3>
                </div>
                <div class="card-body">
                  <form role="form" method="post" action="{{ route('researches.update',$research) }}">
                    @csrf
                    <div class="row">
                      <div class="col-sm-5">
                        <div class="form-group">
                          <label>Research Name </label>
                          <input name ="name" type="text" class="form-control" placeholder="Research name here" value="{{$research->name}}">
                        </div>
                      </div>
                         
                      <div class="col-sm-1"> 
                      </div>    

                      <div class="col-sm-5">
                        <div class="form-group">
                          <label>Research Description:</label>
                          <textarea name ="description" class="form-control" rows="3" placeholder="About the Research" value="{{$research->description}}"> </textarea>
                        </div>
                      </div>
                  </div>

                    <div class="row">
                      <div class="col-sm-5">
                        <div class="form-group">
                          <label>Research Status</label>
                           <select name = "status" class="form-control">
                          <option>{{$research->status}}</option>
                          <option>Ongoing</option>
                          <option>Completed</option>
                        </select>
                        </div>
                      </div>

                      <div class="col-sm-1"> 
                      </div>  

                      <div class="col-sm-5">
                        <div class="form-group">
                          <label>Module</label>
                        <select name = "module" class="form-control">
                          <option>{{$research->module}}</option>
                          <option>CCP</option>
                          <option>HCI</option>
                          <option>ITP</option>
                          <option>IWT</option>
                          <option>IUP</option>
                        </select>
                        </div>
                      </div>
                    </div>
                       
                    <div class="row">
                      <div class="col-sm-5">
                          <div class="form-group">
                              <label>Started Date</label>
                              <input name = "start_date" type="date" class="form-control"  data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask value="{{$projects->started_date}}">
                          </div>
                        </div>

                      <div class="col-sm-1"> 
                      </div>  

                      <div class="col-sm-5">
                        <div class="form-group">
                         <label>Batch (Year / Semester) </label>
                            <select name = "batch" class="form-control">
                              <option>{{$research->batch}}</option>
                              <option>Year 1 Semester 1</option>
                              <option>Year 1 Semester 2</option>
                              <option>Year 2 Semester 1</option>
                              <option>Year 2 Semester 2</option>
                              <option>Year 3 Semester 1</option>
                              <option>Year 3 Semester 2</option>
                              <option>Year 4 Semester 1</option>
                              <option>Year 4 Semester 2</option>
                            </select>
                        </div>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-sm-5">
                         <div class="form-group">
                            <label>Completed Date</label>
                            <input name = "complete_date" type="date" class="form-control"  data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask value="{{$research->complete_date}}">
                        </div>
                       </div>
                     
                      <div class="col-sm-1"> 
                      </div>  

                     <div class="col-sm-5">
                        <div class="form-group">
                          <label>Estimated Time Period</label>
                            <select name = "estimate_time" class="form-control">
                              <option>{{$research->estimate_time}}</option>
                              <option>4 weeks</option>
                              <option>6 weeks</option>
                              <option>8 weeks</option>
                              <option>10 weeks</option>
                              <option>12 weeks</option>
                              <option>20 weeks</option>
                          </select>
                        </div>
                      </div>
  
                      <div class="col-sm-1"> 
                      </div> 

                    </div> 

                      <br>
                      <div class="row">
                      <div class="col-sm-5">

                      <td>
                        <button type="submit" class="btn btn-block btn-success btn-sm">SAVE</button>
                      </td>
                      <br>
                     </div>
                 </div>
             </form>
         </div>
     </div>
  </div>
</div>


<style type="text/css">
h2 {
  color: #000000;
  text-align: center;
  text-transform: uppercase;
  text-shadow: 2px 2px #d8c2c2;
}
</style>
@endsection

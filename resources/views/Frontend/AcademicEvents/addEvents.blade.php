@extends('frontend.layouts.main')
@section('content')

 <!-- Font Awesome -->
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- fullCalendar -->
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-daygrid/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-timegrid/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-bootstrap/main.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/almasaeed2010/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    
  <div class="content-body">
    <h2> Academic Events</h2>
	<div class="col-md-12">
	            <!-- general form elements disabled -->
	            <div class="card card-warning">
	              <div class="card-header">
	                <h3 class="card-title">Add Academic Events</h3>
	              </div>
	              <!-- /.card-header -->
	              <div class="card-body">
	                <form role="form" method="post" action="{{ route('academic_events.stores') }}">
	                	@csrf
	                  <div class="row">
	                    <div class="col-sm-5">
	                      <div class="form-group">
	                        <label>Event Name:</label>
	                        <input name="ename" type="text" class="form-control" placeholder="Event name here">
	                      </div>
	                    </div>
	                       
	                    <div class="col-sm-1"> 
	                    </div>    

	                  	<div class="col-sm-5">
	                      <div class="form-group">
	                        <label>Event Description:</label>
	                        <textarea class="form-control" rows="3" name="edesp" placeholder="About the event"></textarea>
	                      </div>
	                    </div>
 					  </div>

 					  <div class="row">
	                    <div class="col-sm-5">
	                      <div class="form-group">
	                        <label>Event Organizer:</label>
	                        <input name="organizer" type="text" class="form-control" placeholder="Organizer name here">
	                      </div>
	                    </div>
	                 

	                 	<div class="col-sm-1"> 
	                    </div>    

	                    <div class="col-sm-5">
	                   	    <div class="form-group">
	                  	       <label>Event Date</label>
								<input name="edate" type="date" class="form-control"  data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask>
	                		</div>
	                   		</div>
	               		</div>
	               		<br>
	               		<div class="row">
	                	  <div class="col-sm-5">
	                	  	<div class="bootstrap-timepicker">
	                   		<div class="form-group">
	                  			<label>Event time:</label>
									 <div class="input-group date" id="timepicker" data-target-input="nearest">
                      				<input name="etime" type="text" class="form-control datetimepicker-input" data-target="#timepicker"/>
                      				<div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                          			<div class="input-group-text"><i class="far fa-clock"></i></div>
                      				</div>
                      				</div>
                      			</div>
	                		</div>
	                		</div>
						</div>
						<br>
						<div class="row">
						<div class="col-sm-5">
	                    <td>
	                      <button type="submit" class="btn btn-block btn-success btn-sm">Submit</button>
	                    </td>
	                   </div>
	               </div>
	           </form>
	       </div>
	   </div>
	</div>
</div>

<style type="text/css">
h2 {
  color: #000000;
  text-align: center;
  text-transform: uppercase;
  text-shadow: 2px 2px #d8c2c2;
}
</style>

@endsection

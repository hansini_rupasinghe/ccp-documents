@extends('frontend.layouts.main')
@section('content')

<div class="table-responsive">
<table class="table table-striped">  
  <thead>  
    <tr>   
      <th>Event ID </th>  
      <th>Event name </th>  
      <th>Event description </th>  
      <th>Organizer</th>  
      <th>Event date </th>  
      <th>Event time</th>
    </tr>  
  </thead>  
<tbody>  
@foreach($events as $eve)  
        <tr border="none">  
          <td>{{$eve->id}}</td>  
          <td>{{$eve->ename}}</td>  
          <td>{{$eve->edesp}}</td>  
          <td>{{$eve->organizer}}</td>  
          <td>{{$eve->edate}}</td>  
          <td>{{$eve->etime}}</td>
          <td>  
            <form action="{{ route('academic_events.delete', $eve->id)}}" method="post">  
              @csrf  
             <!-- // @method('DELETE')  -->
              <button class="btn btn-danger" type="submit">Delete</button>  
            </form>
          </td>  
  
        </tr>  
@endforeach  
</tbody>  
</table>
</div>  
@endsection  
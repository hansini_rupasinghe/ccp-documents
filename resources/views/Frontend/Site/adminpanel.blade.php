@extends('frontend.layouts.main')
@section('content')

<section class="content">

  <h1>Admin Panel</h1>
  <br>

  <div class="container-fluid">

    <div class="row">
      
      <div class="col-lg-3 col-6">

        <div class="small-box bg-success">
          <div class="inner">
            <h4>Log Report</h4>

            <p></p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="{{ url('/site/log') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-danger">
          <div class="inner">

            <h4>Users Report</h4>
          </div>
          <div class="icon">
            <i class="ion ion-pie-graph"></i>
          </div>
          <a href="{{ url('/site/users') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-danger">
          <div class="inner">

            <h4>Faculties & Departments</h4>
          </div>
          <div class="icon">
            <i class="ion ion-pie-graph"></i>
          </div>
          <a href="{{ url('/faculty_department/adminfaculty') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-3 col-6">
        <div class="small-box bg-danger">
          <div class="inner">

            <h4>Modules</h4>
          </div>
          <div class="icon">
            <i class="ion ion-pie-graph"></i>
          </div>
          <a href="{{ url('/module/adminmodule') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
    
  </div>
</section>


<style type="text/css">
h1 {
  /*color: #007bff;*/
  text-align: center
}
</style>

@endsection
@extends('frontend.layouts.main')
@section('content')
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- fullCalendar -->
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-daygrid/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-timegrid/main.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fullcalendar-bootstrap/main.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/almasaeed2010/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <section class="content">
     <h1>All Modules</h1>
      <div class="container-fluid">
      
        <!-- Small boxes (Stat box) -->
        <div class="row">
        <!-- /.row -->
        <!-- Main row -->

        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                       
                        <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
                    </div>
                                
                       <div class="table-responsive">
                          <table class="table table-striped">
                             <thead>
                               <tr>
                                 <th>Module ID</th>
                                 <th>Module Code</th>
                                 <th>Module Name</th>
                                 <th>Faculty</th>
                                 <th>Department</th>
                                 <th>Year</th>
                                 <th>Semester</th>
                                 <th>Maximum Number of Lecturers</th>
                                 <th>Lecture Hours</th>
                                 <th>Tutorial Hours</th>
                                 <th>Laboratory Hours</th>
                                 <th>Credit Hours</th>
                                 <th>Action</th>
                                 <th></th>

                               </tr>
                             </thead>

                                  <tbody>
                                    @foreach($modules as $tt)  
                                    <tr border="none">  
                                      <td>{{$tt->id}}</td>
                                      <td>{{$tt->mod_code}}</td>   
                                      <td>{{$tt->mod_name}}</td> 
                                      <td>{{$tt->faculty}}</td> 
                                      <td>{{$tt->department}}</td>  
                                      <td>{{$tt->year}}</td> 
                                      <td>{{$tt->semester}}</td> 
                                      <td>{{$tt->max_lec}}</td>  
                                      <td>{{$tt->lec_hours}}</td> 
                                      <td>{{$tt->tute_hours}}</td> 
                                      <td>{{$tt->lab_hours}}</td>  
                                      <td>{{$tt->credit_hours}}</td>  
                                      <td>  
                                        <form action="{{ route('module.delete', $tt->id)}}" method="post">  
                                          @csrf  
                                         <!-- // @method('DELETE')  -->
                                          <button class="btn btn-danger" type="submit">Delete</button>  
                                        </form>
                                      </td>  
                                      <td >  
                                        <form action="{{ route('module.edit', [$tt->id])}}" method="GET">  
                                          @csrf  
                                           
                                          <button class="btn btn-danger" type="submit">Edit</button>  
                                        </form>
                                      </td>  
                              
                                    </tr>  
                                  @endforeach  
                                  </tbody>

                          </table>
                        </div>
                </div>
            </div>
         </div>
    </section>

    <style type="text/css">
h1 {
  color: #ffffff;
  background-color: #6500F9;
}
</style>

@endsection
@extends('frontend.layouts.main')
@section('content')
  
  <link rel="stylesheet" href="/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/almasaeed2010/adminlte/dist/css/adminlte.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

<div class="content-body">
  <h2> Modules </h2>
  <div class="col-md-12">
    <div class="card card">
      <div class="card-header" style="background-color: #6500F9 !important;">
        <h3 class="card-title">Add Modules</h3>
      </div>

      <div class="card-body">
        <form role="form" method="post" action="{{ route('module.store') }}">
          @csrf
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Module Code</label>
                <input name="mod_code" type="text" class="form-control" placeholder="Module Code Here">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Module Name</label>
                <input name="mod_name" type="text" class="form-control" placeholder="Module Name Here">
              </div>
            </div>
                    
             <div class="col-sm-6">
              <div class="form-group">
                <label>Select Faculty</label>
                <select name="faculty" class="form-control">
                  <option>--Select--</option>
                  <option>Faculty of Engineering</option>
                  <option>Faculty of Computing</option>
                  <option>Faculty of Business</option>
                  <option>Faculty of Humanities & Sciences</option>
                </select>
              </div>
            </div>

          </div> 

          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Select Department</label>
                <select name="department" class="form-control">
                  <option>--Select--</option>
                  <option value="1">DEPARTMENT OF INFORMATION TECHNOLOGY</option>
                  <option value="2">DEPARTMENT OF COMPUTER SCIENCE & SOFTWARE ENGINEERING</option>
                  <option value="3">DEPARTMENT OF COMPUTER SYSTEMS ENGINEERING</option>
                  <option value="4">DEPARTMENT OF MECHANICAL ENGINEERING</option>
                </select>
              </div>
            </div>
                    
             <div class="col-sm-6">
              <div class="form-group">
                <label>Select Year</label>
                <select name="year" class="form-control">
                  <option>--Select--</option>
                  <option>01</option>
                  <option>02</option>
                  <option>03</option>
                  <option>04</option>
                </select>
              </div>
            </div>

          </div> 

            <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Select Semester</label>
                <select name="semester" class="form-control">
                  <option>--Select--</option>
                  <option>01</option>
                  <option>02</option>
                </select>
              </div>
            </div>
                    
             <div class="col-sm-6">
              <div class="form-group">
                <label>Select Maximum Number of Lecturers</label>
                <select name="max_lec" class="form-control">
                   <option>--Select--</option>
                  <option>01</option>
                  <option>02</option>
                  <option>03</option>
                  <option>04</option>
                </select>
              </div>
            </div>

          </div> 

            <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Select Lecture Hours</label>
                <select name="lec_hours" class="form-control">
                  <option>--Select--</option>
                  <option>10</option>
                  <option>20</option>
                  <option>30</option>
                  <option>40</option>
                </select>
              </div>
            </div>
                    
             <div class="col-sm-6">
              <div class="form-group">
                <label>Select Tutorial Hours</label>
                <select name="tute_hours" class="form-control">
                  <option>--Select--</option>
                  <option>10</option>
                  <option>20</option>
                  <option>30</option>
                  <option>40</option>
                </select>
              </div>
            </div>

          </div> 

            <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Select Laboratory Hours</label>
                <select name="lab_hours" class="form-control">
                  <option>--Select--</option>
                  <option>10</option>
                  <option>20</option>
                  <option>30</option>
                  <option>40</option>
                </select>
              </div>
            </div>
                    
             <div class="col-sm-6">
              <div class="form-group">
               <label>Select Credit Hours</label>
                <select name="credit_hours" class="form-control">
                  <option>--Select--</option>
                  <option>0</option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                </select>
              </div>
            </div>

          </div> 
          <div class="row">
            <div class="general-button">
              <button type="submit" class="btn mb-1 btn-success">Save</button>
            </div>
          </div>
        </form>
      </div>   
    </div>
  </div>
</div>
        

<style type="text/css">
h2 {
  color: #000000;
  text-align: center;
  text-transform: uppercase;
  text-shadow: 2px 2px #d8c2c2;
}
</style>
            
@endsection
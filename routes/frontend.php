<?php

Route::get('/index','SiteController@index'); 
Route::get('/site/adminpanel','SiteController@adminPanel');
Route::get('/site/log','SiteController@logReport');
Route::get('/site/users','SiteController@userReport');

Route::get('/time/view','OTimetableController@view');
Route::get('/time/create','OTimetableController@create');
// --------------------------------------------------------------------------
Route::get('/timetable/index','TimetableController@index')->name('timetable.index');
Route::get('/timetable/create','TimetableController@create')->name('timetable.create');
Route::post('/timetable/store','TimetableController@store')->name('timetable.store');
Route::get('/timetable/edit/{id}','TimetableController@edit')->name('timetable.edit');
Route::post('/timetable/update/{id}','TimetableController@update')->name('timetable.update');
Route::post('/timetable/delete/{id}','TimetableController@destroy')->name('timetable.delete');
Route::get('/timetable/view/{id}','TimetableController@show')->name('timetable.view');
// --------------------------------------------------------------------------O
Route::get('/login','SiteController@login');
Route::get('/signup','SiteController@signup');
Route::get('/welcome','SiteController@welcome');
Route::get('/academic_events/add_events','AcademicEventController@addEvents');
Route::get('/academic_events/view_events','AcademicEventController@viewEvents');

// Route::get('/projects/add','ProjectController@add');
// Route::get('/projects/view','ProjectController@view');
// Route::get('/researches/add','ResearchController@add');
// Route::get('/researches/view','ResearchController@view');
// Route::get('/publications/add','PublicationController@add');
// Route::get('/publications/view','PublicationController@view');

Route::get('/projects/index','ProjectsController@index');
Route::get('/projects/create','ProjectsController@create');
Route::post('/projects/store','ProjectsController@store')->name('projects.store');
Route::get('/projects/edit/{id}','ProjectsController@edit')->name('projects.edit');
Route::post('/projects/update/{id}','ProjectsController@update')->name('projects.update');
Route::post('/projects/delete/{id}','ProjectsController@delete')->name('projects.delete');
//Route::post('/projects/show{id}','ProjectsController@store')->name('projects.show');
 
//Route::get('/researches/add','ResearchesController@add')->name('researches.view');
Route::get('/researches/index','ResearchesController@index');
Route::get('/researches/create','ResearchesController@create');
Route::post('/researches/store','ResearchesController@store')->name('researches.store');
Route::get('/researches/edit/{id}','ResearchesController@edit')->name('researches.edit');
Route::post('/researches/update/{id}','ResearchesController@update')->name('researches.update');
Route::post('/researches/delete/{id}','ResearchesController@delete')->name('researches.delete');

//Route::get('/publications/add','PublicationsController@add')->name('publications.view');
Route::get('/publications/index','PublicationsController@index');
Route::get('/publications/create','PublicationsController@create');
Route::post('/publications/store','PublicationsController@store')->name('publications.store');
Route::get('/publications/edit/{id}','PublicationsController@edit')->name('publications.edit');
Route::post('/publications/update/{id}','PublicationsController@update')->name('publications.update');
Route::post('/publications/delete/{id}','PublicationsController@delete')->name('publications.delete');

Route::get('/faculty_department/faculty','FacultyDepartmentController@view');
Route::get('/faculty_department/adminfaculty','FacultyDepartmentController@adminView');
Route::get('/faculty_department/addfaculty','FacultyDepartmentController@addFaculty');
Route::get('/faculty_department/adddepartment','FacultyDepartmentController@addDepartment');
Route::get('/module/module','ModuleController@view');
Route::get('/module/adminmodule','ModuleController@adminView');
Route::get('/module/addmodule','ModuleController@addModule');

Route::get('/biography/view', 'BiographyController@view');
Route::get('/biography/add', 'BiographyController@addpersonal');
Route::post('/uploadfile/add','BiographyController@add');
Route::get('/exp/add','ExpController@exp');
Route::get('/edu/add','EduController@edu');
Route::get('/accomp/add','AccompController@accomp');
Route::get('/skills/add','SkillsController@skills');
Route::get('/work/add','WorkController@work');
Route::get('/work/view','WorkController@viewwork');

?> 
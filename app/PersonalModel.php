<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalModel extends Model
{
    public $timestamps = false;
    
    protected $table='personal_info';

    protected $fillable=[
        'full_name', 'resident_address', 'workplace_address', 'nic', 'gender', 'email', 'dob', 'contact',
    ]; 
}

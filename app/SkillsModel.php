<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkillsModel extends Model
{
    public $timestamps = false;
    
    protected $table='skills';

    protected $fillable=[
       'userid', 'skills', 'desp','endorsements' 
    ]; 
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentModel extends Model
{
    public $timestamps = false;
    
    protected $table='departments';

    protected $fillable=[
        'id', 'dept_name', 'faculty'
    ]; 
}

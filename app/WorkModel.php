<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkModel extends Model
{
    public $timestamps = false;
    
    protected $table='workshops';

    protected $fillable=[
        'workshop', 'event_name','venue', 'event_date', 'event_time', 
    ]; 
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timetable extends Model
{
    public $timestamps = false;
    
    protected $table='timetable';

    protected $fillable=[
        'ddate','module','category','tot_hours'
    ];  
}

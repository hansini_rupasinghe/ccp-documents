<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectsModel extends Model
{
     public $timestamps = false;
    
    protected $table='projects';

    protected $fillable=[
        'id', 'topic', 'description', 'project_type', 'started_date', 'completed_date', 'module', 'estimate_time', 'batch'
    ]; 
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpModel extends Model
{
    public $timestamps = false;
    
    protected $table='working_exp';

    protected $fillable=[
        'userid','designation', 'organization',	'years', 'start_date', 'remarks',
    ]; 
}


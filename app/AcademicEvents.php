<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademicEvents extends Model
{
    public $timestamps = false;
    
    protected $table='events';

    protected $fillable=[
        'ename','edesp','organizer','edate','etime'
    ];  
}

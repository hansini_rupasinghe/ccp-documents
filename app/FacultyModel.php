<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacultyModel extends Model
{
    public $timestamps = false;
    
    protected $table='faculty';

    protected $fillable=[
        'id', 'name', 'desp'
    ]; 
}

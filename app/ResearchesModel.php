<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResearchesModel extends Model
{
     public $timestamps = false;
    
    protected $table='research';

    protected $fillable=[
        'id', 'name', 'description', 'status', 'module', 'start_date', 'complete_date', 'batch', 'estimate_time'
    ]; 
}

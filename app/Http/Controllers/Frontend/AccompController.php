<?php

namespace App\Http\Controllers\Frontend;

use App\AccompModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccompController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $accomplishments = AccompModel::all();

         return view('Frontend.Biography.view', compact('accomplishments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Frontend.Biography.accomp');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $ac = new AccompModel;  
        $ac->title =  $request->get('title');  
       // $pd->prof_img = $request->get('prof_img');  
        $ac->desp = $request->get('desp');
        $ac->userid = 'vdv';

        $ac->save();

         return redirect()->route('accomplishments.view')
                        ->with('success','Details created successfully.');  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AccompModel  $accompModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $accomplishments = AccompModel::find($id);
        return view('Frontend.Biography.view', compact('accomplishments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AccompModel  $accompModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $accomplishments= AccompModel::find($id);  
        return view('Frontend.Biography.update', compact('accomplishments'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AccompModel  $accompModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $ac = AccompModel::find($id);  
         $ac->title =  $request->get('title');  
       // $pd->prof_img = $request->get('prof_img');  
        $ac->desp = $request->get('desp');
        

        $ac->save();

         return redirect()->route('accomplishments.view')
                        ->with('success','Details created successfully.');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AccompModel  $accompModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ac=AccompModel::find($id);  
        $ac->delete();

         $accomplishments = AccompModel::all();
         return view('Frontend.Biography.view', compact('accomplishments'));

    }
}

<?php

namespace App\Http\Controllers\Frontend;

use App\WorkModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $workshops = WorkModel::all();

         return view('Frontend.Biography.viewwork', compact('workshops'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Frontend.Biography.work');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $wk = new WorkModel;  
        $wk->workshop =  $request->get('workshop');  
       // $pd->prof_img = $request->get('prof_img');  
        $wk->event_name = $request->get('event_name');
        $wk->venue = $request->get('venue');
        $wk->event_date = $request->get('event_date');
        $wk->event_time = $request->get('event_time');
        $wk->userid = 'vdv';
        $wk->save();

         return redirect()->route('workshops.view')
                        ->with('success','Details created successfully.'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WorkModel  $workModel
     * @return \Illuminate\Http\Response
     */
    public function show(WorkModel $workModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WorkModel  $workModel
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkModel $workModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WorkModel  $workModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkModel $workModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WorkModel  $workModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wk=WorkModel::find($id);  
        $wk->delete();

         $workshops = WorkModel::all();

         return view('Frontend.Biography.viewwork', compact('workshops'));
    }
}

<?php

namespace App\Http\Controllers\Frontend;

use App\EduModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EduController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $educational_qualification = EduModel::all();

         return view('Frontend.Biography.view', compact('educational_qualification'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Frontend.Biography.edu');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([  
            'start_date'=>'required', 'end_date' => 'required', 
            // 'last_name'=>'required',  
            // 'gender'=>'required',  
            // 'qualifications'=>'required'  
        ]);  

        $eq = new EduModel;  
        $eq->institute =  $request->get('institute');  
       // $pd->prof_img = $request->get('prof_img');  
        $eq->degree = $request->get('degree');
        $eq->field = $request->get('field');
        $eq->start_date = $request->get('start_date');
        $eq->end_date = $request->get('end_date');
        $eq->userid = 'vdv';
        $eq->save();

         return redirect()->route('educational_qualification.view')
                        ->with('success','Details created successfully.');  

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EduModel  $eduModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $educational_qualification = EduModel::find($id);
        return view('Frontend.Biography.view', compact('educational_qualification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EduModel  $eduModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $educational_qualification = EduModel::find($id);
        return view('Frontend.Biography.view', compact('educational_qualification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EduModel  $eduModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([  
            'start_date'=>'required', 'end_date' => 'required', 
            // 'last_name'=>'required',  
            // 'gender'=>'required',  
            // 'qualifications'=>'required'  
        ]);  

         $eq = EduModel::find($id);  
         $eq->institute =  $request->get('institute');  
       // $pd->prof_img = $request->get('prof_img');  
        $eq->degree = $request->get('degree');
        $eq->field = $request->get('field');
        $eq->start_date = $request->get('start_date');
        $eq->end_date = $request->get('end_date');

        $eq->save();

         return redirect()->route('educational_qualification.view')
                        ->with('success','Details created successfully.');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EduModel  $eduModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eq=EduModel::find($id);  
        $eq->delete();

         $educational_qualification = EduModel::all();
          return view('Frontend.Biography.view', compact('educational_qualification'));
    }
}

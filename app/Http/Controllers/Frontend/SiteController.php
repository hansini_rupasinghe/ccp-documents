<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class SiteController extends Controller
{
    public function index(){
    	return view('Frontend.Site.index');
    }

    public function adminPanel(){
    	return view('Frontend.Site.adminpanel');
    }

    public function logReport()
    {
        // if(Auth::user()->user_type == [0,7,8,9]){
            $logs = DB::select('SELECT system_log.*, users.name FROM system_log INNER JOIN users ON system_log.user_id = users.user_code');

            return view('Frontend.Site.logview', compact('logs'));
        // }
        // else{
        //     abort(403, 'Unauthorized action.');
        // }
    }

    public function login(){
    	return view('Frontend.Site.login');
    }

    public function signup(){
        return view('Frontend.Site.signup');
    }

    public function welcome(){
        return view('Frontend.Site.welcome');
    }

    public function userReport()
    {
        // if(Auth::user()->user_type == [0,7,8,9]){
            $users = DB::select('SELECT users.*, designations.desp FROM users INNER JOIN designations ON users.user_type = designations.id');

            return view('Frontend.Site.users_index', compact('users'));
        // }
        // else{
        //     abort(403, 'Unauthorized action.');
        // }
    }
}

?>
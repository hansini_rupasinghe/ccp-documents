<?php

namespace App\Http\Controllers\Frontend;

use App\SkillsModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SkillsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $skills = SkillsModel::all();

         return view('Frontend.Biography.view', compact('skills'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('Frontend.Biography.skills');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sk = new SkillsModel;  
        $sk->skills =  $request->get('skills');  
       // $pd->prof_img = $request->get('prof_img');  
        $sk->desp = $request->get('desp');
        $sk->endorsements = $request->get('endorsements');
        $sk->userid = 'vdv';

        $sk->save();

         return redirect()->route('skills.view')
                        ->with('success','Details created successfully.');  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SkillsModel  $skillsModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $skills = SkillsModel::find($id);
        return view('Frontend.Biography.view', compact('skills'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SkillsModel  $skillsModel
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
         $skills = SkillsModel::find($id);
        return view('Frontend.Biography.view', compact('skills'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SkillsModel  $skillsModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {

        $sk = SkillsModel::find($id);  
        $sk->skills =  $request->get('skills');  
       // $pd->prof_img = $request->get('prof_img');  
        $sk->desp = $request->get('desp');
        $sk->endorsements = $request->get('endorsements');
     

        $sk->save();

         return redirect()->route('skills.view')
                        ->with('success','Details created successfully.');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SkillsModel  $skillsModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $sk=SkillsModel::find($id);  
         $sk->delete();

         $skills = SkillsModel::all();
         return view('Frontend.Biography.view', compact('skills'));
    }
}

<?php

namespace App\Http\Controllers\Frontend;

use App\PublicationsModel;
use App\Module;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PublicationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publications = PublicationsModel::all();

        return view('Frontend.Publications.view', compact('publications') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        return view('Frontend.Publications.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $tt = new PublicationsModel;  
        $tt->name =  $request->get('name');
        $tt->pub_description =  $request->get('pub_description');  
        $tt->publication_type = $request->get('publication_type');  
        $tt->pub_date = $request->get('pub_date');
        $tt->pub_version = $request->get('pub_version');  
        $tt->save();

        return redirect()->route('publications.view')
                        ->with('success','Product created successfully.'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PublicationsModel  $publicationsModel
     * @return \Illuminate\Http\Response
     */
    public function show(PublicationsModel $publicationsModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PublicationsModel  $publicationsModel
     * @return \Illuminate\Http\Response
     */
    public function edit(PublicationsModel $publicationsModel)
    {
        $publications= PublicationsModel::find($id);  
        return view('Frontend.Publications.update', compact('publications'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PublicationsModel  $publicationsModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PublicationsModel $publicationsModel)
    {
        $tt = new PublicationsModel;  
        $tt->name =  $request->get('name');
        $tt->pub_description =  $request->get('pub_description');  
        $tt->publication_type = $request->get('publication_type');  
        $tt->pub_date = $request->get('pub_date');
        $tt->pub_version = $request->get('pub_version');  
        $tt->save();

        return redirect()->route('publications.view')
                        ->with('success','Product created successfully.'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PublicationsModel  $publicationsModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(PublicationsModel $publicationsModel)
    {
        //
    }
}

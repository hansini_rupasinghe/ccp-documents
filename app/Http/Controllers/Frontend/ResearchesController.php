<?php

namespace App\Http\Controllers\Frontend;

use App\ResearchesModel;
use App\Module;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResearchesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $research = ResearchesModel::all();

        return view('Frontend.Researches.view', compact('research') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Frontend.Researches.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $tt = new ResearchesModel;  
        $tt->name =  $request->get('name');  
        $tt->description = $request->get('description');  
        $tt->status = $request->get('status');
        $tt->module = $request->get('module');  
        $tt->start_date = $request->get('start_date');
        $tt->complete_date = $request->get('complete_date');
        $tt->batch = $request->get('batch');
        $tt->estimate_time = $request->get('estimate_time');
        $tt->save();

        return redirect()->route('researches.view')
                        ->with('success','Product created successfully.'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ResearchesModel  $researchesModel
     * @return \Illuminate\Http\Response
     */
    public function show(ResearchesModel $researchesModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ResearchesModel  $researchesModel
     * @return \Illuminate\Http\Response
     */
    public function edit(ResearchesModel $researchesModel)
    {
        $research= ResearchesModel::find($id);  
        return view('Frontend.Researches.update', compact('research'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ResearchesModel  $researchesModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ResearchesModel $researchesModel)
    {
        $tt = ResearchesModel::find($id);  
        $tt->name =  $request->get('name');  
        $tt->description = $request->get('description');  
        $tt->status = $request->get('status');
        $tt->module = $request->get('module');  
        $tt->start_date = $request->get('start_date');
        $tt->complete_date = $request->get('complete_date');
        $tt->batch = $request->get('batch');
        $tt->estimate_time = $request->get('estimate_time');

        $tt->save();

        return redirect()->route('researches.view')
                        ->with('success','Product created successfully.'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ResearchesModel  $researchesModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResearchesModel $researchesModel)
    {
        //
    }
}

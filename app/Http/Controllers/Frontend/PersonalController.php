<?php

namespace App\Http\Controllers\Frontend;

use App\PersonalModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PersonalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personal_info = PersonalModel::all();  

        return view('Frontend.Biography.view', compact('personal_info'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Frontend.Biography.personal');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([  
            'dob'=>'required',  
            // 'last_name'=>'required',  
            // 'gender'=>'required',  
            // 'qualifications'=>'required'  
        ]);  
  
        $pd = new PersonalModel;  
        $pd->full_name =  $request->get('full_name');  
       // $pd->prof_img = $request->get('prof_img');  
        $pd->resident_address = $request->get('resident_address');
        $pd->workplace_address = $request->get('workplace_address');
        $pd->nic = $request->get('nic');
        $pd->gender = $request->get('gender');
        $pd->email = $request->get('email');
        $pd->dob = $request->get('dob');
        $pd->contact = $request->get('contact');
        $pd->user_id = 'vdv';
        $pd->save();

         return redirect()->route('personal_info.view')
                        ->with('success','Details created successfully.');  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PersonalModel  $personalModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $personal_info = PersonalModel::find($id);
        return view('Frontend.Biography.view', compact('personal_info'));   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PersonalModel  $personalModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $personal_info = PersonalModel::find($id);
        return view('Frontend.Biography.view', compact('personal_info'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PersonalModel  $personalModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $request->validate([  
            'dob'=>'required',  
            // 'last_name'=>'required',  
            // 'gender'=>'required',  
            // 'qualifications'=>'required'  
        ]);  

          $pd = PersonalModel::find($id);  
           $pd->full_name =  $request->get('full_name');  
       // $pd->prof_img = $request->get('prof_img');  
         $pd->resident_address = $request->get('resident_address');
        $pd->workplace_address = $request->get('workplace_address');
        $pd->nic = $request->get('nic');
        $pd->gender = $request->get('gender');
        $pd->email = $request->get('email');
        $pd->dob = $request->get('dob');
        $pd->contact = $request->get('contact');

         $pd->save();

         return redirect()->route('personal_info.view')
                        ->with('success','Details created successfully.');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PersonalModel  $personalModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $pd=PersonalModel::find($id);  
        $pd->delete();

         $personal_info = PersonalModel::all();
          return view('Frontend.Biography.view', compact('personal_info'));

    }
}

<?php

namespace App\Http\Controllers\Frontend;

use App\ProjectsModel;
use App\Module;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = ProjectsModel::all();

        return view('Frontend.Projects.view', compact('projects') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Frontend.Projects.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       /* $request->validate([  
            //'ddate'=>'required',
        ]); 
        */ 
  
        $tt = new ProjectsModel;  
        $tt->topic =  $request->get('topic');  
        $tt->description = $request->get('description');  
        $tt->project_type = $request->get('project_type');
        $tt->started_date = $request->get('started_date');  
        $tt->completed_date = $request->get('completed_date');
        $tt->module = $request->get('module');
        $tt->estimate_time = $request->get('estimate_time');
        $tt->batch = $request->get('batch');
        $tt->save();

        return redirect()->route('projects.view')
                        ->with('success','Product created successfully.'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProjectsModel  $projectsModel
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectsModel $projectsModel)
    {
        $projects= ProjectsModel::find($id);  
        return view('Frontend.Projects.view', compact('projects'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectsModel  $projectsModel
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectsModel $projectsModel)
    {
        $projects= ProjectsModel::find($id);  
        return view('Frontend.Projects.view', compact('projects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectsModel  $projectsModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectsModel $projectsModel)
    {
        $tt = new ProjectsModel;  
        $tt->topic =  $request->get('topic');  
        $tt->description = $request->get('description');  
        $tt->project_type = $request->get('project_type');
        $tt->started_date = $request->get('started_date');  
        $tt->completed_date = $request->get('completed_date');
        $tt->module = $request->get('module');
        $tt->estimate_time = $request->get('estimate_time');
        $tt->batch = $request->get('batch');
        $tt->save();

        return redirect()->route('projects.view')
                        ->with('success','Product created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectsModel  $projectsModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectsModel $projectsModel)
    {
        $tt=ProjectsModel::find($id);
        $tt->delete();

        $projects = ProjectsModel::all();
        return view('Frontend.Projects.view', compact('projects'));
    }
}

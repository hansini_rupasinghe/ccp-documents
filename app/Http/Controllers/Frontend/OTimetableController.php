<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OTimetableController extends Controller
{
    public function view(){
    	return view('Frontend.Timetable.view');
    }

    public function create(){
    	return view('Frontend.Timetable.tt_form');
    }

}

?>
<?php

namespace App\Http\Controllers\Frontend;

use App\FacultyModel;
use App\DepartmentModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FacultyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faculty = FacultyModel::all();
        $departments = DepartmentModel::all();


        return view('Frontend.Faculty_Department.adminFaculty', compact('faculty','departments') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Frontend.Faculty_Department.addFaculty');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tt = new FacultyModel;  
        $tt->name =  $request->get('name');  
        $tt->desp = $request->get('desp');  
        $tt->save();

        return redirect()->route('Adminfaculty.view')
                        ->with('success','Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FacultyModel  $facultyModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $faculty= DepartmentModel::find($id);  
        return view('Frontend.Faculty_Department.adminFaculty', compact('faculty'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FacultyModel  $facultyModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faculty= FacultyModel::find($id);  
        return view('Frontend.Faculty_Department.updateFaculty', compact('faculty'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FacultyModel  $facultyModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tt = FacultyModel::find($id);  
        $tt->name =  $request->get('name');  
        $tt->desp = $request->get('desp');  
        $tt->save();

        return redirect()->route('Adminfaculty.view')
                        ->with('success','Product created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FacultyModel  $facultyModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tt=FacultyModel::find($id);  
        $tt->delete();

       $departments = DepartmentModel::all();
       $faculty = FacultyModel::all();

        return view('Frontend.Faculty_Department.adminFaculty', compact('departments','faculty') );
    }
}

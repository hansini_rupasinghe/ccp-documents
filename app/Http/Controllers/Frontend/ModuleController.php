<?php

namespace App\Http\Controllers\Frontend;

use App\ModuleModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = ModuleModel::all();

        return view('Frontend.Module.adminModule', compact('modules') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        return view('Frontend.Module.addModule');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$request->validate([  
            'ddate'=>'required',
        ]);*/  
  
        $tt = new ModuleModel;  
        $tt->mod_code =  $request->get('mod_code');  
        $tt->mod_name = $request->get('mod_name');  
        $tt->department = $request->get('department');
        $tt->year = $request->get('year');  
        $tt->semester = $request->get('semester');
        $tt->lec_hours = $request->get('lec_hours');
        $tt->tute_hours = $request->get('tute_hours');
        $tt->lab_hours = $request->get('lab_hours');
        $tt->faculty = $request->get('faculty');
        $tt->credit_hours = $request->get('credit_hours');
        $tt->max_lec = $request->get('max_lec');
        $tt->save();

        return redirect()->route('module.view')
                        ->with('success','Product created successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModuleModel  $moduleModel
     * @return \Illuminate\Http\Response
     */
    public function show(ModuleModel $moduleModel)
    {
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModuleModel  $moduleModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modules= ModuleModel::find($id);  
        return view('Frontend.Module.updateModule', compact('modules'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModuleModel  $moduleModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tt = ModuleModel::find($id);  
        $tt->mod_code =  $request->get('mod_code');  
        $tt->mod_name = $request->get('mod_name');  
        $tt->department = $request->get('department');
        $tt->year = $request->get('year');  
        $tt->semester = $request->get('semester');
        $tt->lec_hours = $request->get('lec_hours');
        $tt->tute_hours = $request->get('tute_hours');
        $tt->lab_hours = $request->get('lab_hours');
        $tt->faculty = $request->get('faculty');
        $tt->credit_hours = $request->get('credit_hours');
        $tt->max_lec = $request->get('max_lec');

     //    print_r($tt->ddate);
        // die();

        $tt->save();

        return redirect()->route('module.view')
                        ->with('success','Product created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModuleModel  $moduleModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tt=ModuleModel::find($id);  
        $tt->delete();

        $modules = ModuleModel::all();
        return view('Frontend.Module.adminModule', compact('modules'));
    }
}

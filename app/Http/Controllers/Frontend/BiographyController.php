<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BiographyController extends Controller
{
  

	public function view()
	{
		return view('Frontend.Biography.view');
	}

	public function addpersonal()
	{
		return view('Frontend.Biography.personal');
	}

	public function add(Request $request){
		$this->validate($request, ['select_file'	=> 'required|image|mimes:jpeg,png,jpg,gif|max:2048']);
		$image = $request->file('select_file');
		$new_name = rand() . '.' . $image-> getClientOriginalExtension();
		$image->move(public_path("images"), $new_name);
		return back()->with('success', 'Image Uploaded Successfully')->with('path', $new_name);
	}

}

?>
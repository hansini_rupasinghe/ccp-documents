<?php

namespace App\Http\Controllers\Frontend;

use App\DepartmentModel;
use App\FacultyModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = DepartmentModel::all();
        $faculty = FacultyModel::all();

        return view('Frontend.Faculty_Department.adminFaculty', compact('departments','faculty') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Frontend.Faculty_Department.addDepartment');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tt = new DepartmentModel;  
        $tt->dept_name =  $request->get('dept_name');  
        $tt->faculty = $request->get('faculty');  
        $tt->save();

        return redirect()->route('Adminfaculty.view')
                        ->with('success','Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DepartmentModel  $departmentModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          $departments= DepartmentModel::find($id);  
        return view('Frontend.Faculty_Department.adminFaculty', compact('departments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DepartmentModel  $departmentModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departments= DepartmentModel::find($id);  
        return view('Frontend.Faculty_Department.updateDepartment', compact('departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DepartmentModel  $departmentModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tt = DepartmentModel::find($id);  
        $tt->dept_name =  $request->get('dept_name');  
        $tt->faculty = $request->get('faculty');  
        $tt->save();

        return redirect()->route('Adminfaculty.view')
                        ->with('success','Product created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DepartmentModel  $departmentModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tt=DepartmentModel::find($id);  
        $tt->delete();

        $departments = DepartmentModel::all();
        $faculty = FacultyModel::all();

        return view('Frontend.Faculty_Department.adminFaculty', compact('departments','faculty') );
    }
}

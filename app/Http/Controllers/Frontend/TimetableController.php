<?php

namespace App\Http\Controllers\Frontend;

use App\Timetable;
use App\Module;
use App\SystemLog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use DataTables;


class TimetableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $timetable = DB::table('timetable')
            ->join('modules', 'timetable.module', '=', 'modules.id')
            ->where('timetable.userid', Auth::user()->user_code)
            ->select('timetable.*', 'modules.mod_code', 'modules.mod_name')
            ->get();
        //Timetable::where('userid', Auth::user()->user_code)->get(); 

         

        return view('Frontend.Timetable.index', compact('timetable'));

        // return $timetableGrid       ///TimetableGridInterface $timetableGrid, Request $request
        //             ->create(['query' => Timetable::all(), 'request' => $request])
        //             ->renderOn('Frontend.Timetable.index');
    }

    // public function index(Request $request)
    // {
    //     if($request->ajax())
    //     {
    //         $data = DB::table('timetable')
    //                 ->join('modules', 'timetable.module', '=', 'modules.id')
    //                 ->where('timetable.userid', Auth::user()->user_code)
    //                 ->select('timetable.*', 'modules.mod_code', 'modules.mod_name')
    //                 ->get();

    //         return DataTables::of($data)
    //                 ->addColumn('action', function($data){
    //                     $button = '<button type="button" name="edit" id="'.$data->id.'" class="edit btn btn-primary btn-sm">Edit</button>';
    //                     $button .= '&nbsp;&nbsp;&nbsp;<button type="button" name="edit" id="'.$data->id.'" class="delete btn btn-danger btn-sm">Delete</button>';
    //                     return $button;
    //                 })
    //                 ->rawColumns(['action'])
    //                 ->make(true);
    //     }
    //     return view('Timetable.newindex');
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modules = Module::all();

        return view('Frontend.Timetable.create', compact('modules'));
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([  
            'ddate'=>'required',  
            'module'=>'required',  
            'category'=>'required',  
            'tot_hours'=>'required|integer|between:1,200',
        ],[
            'ddate.required'=>'Date is required!',  
            'module.required'=>'Module is required!',  
            'category.required'=>'Category is required!',  
            'tot_hours.required'=>'Total Hours is required!',
            'tot_hours.between'=>'Total Hours must be greater than 0!'
        ]);  

        $tt = new Timetable;
        $tt->ddate =  $request->get('ddate');  
        $tt->module = $request->get('module');  
        $tt->category = $request->get('category');  
        $tt->tot_hours = $request->get('tot_hours');
        $tt->remarks = $request->get('remarks');
        $tt->userid = Auth::user()->user_code;


        $module = Module::find($tt->module)->first();

        $times = Timetable::where('module' , $tt->module)
                        ->where('category' , $tt->category)->get();

        if($times->isEmpty()){

            switch ($tt->category) {
                case 'Lecture':
                    if ($tt->tot_hours > $module->lec_hours) {
                        abort(503, 'Total Hours('.$tt->tot_hours.') exceeds the module lecture hours('.$module->lec_hours.')!');
                    }
                    break;

                case 'Tutorial':
                    if ($tt->tot_hours > $module->tute_hours) {
                        abort(503, 'Total Hours('.$tt->tot_hours.') exceeds the module tutorial hours('.$module->tute_hours.')!');
                    }
                    break;

                case 'Lab Session':
                    if ($tt->tot_hours > $module->lab_hours) {
                        abort(503, 'Total Hours('.$tt->tot_hours.') exceeds the module lab hours('.$module->lab_hours.')!');
                    }
                    break;
                
                // default:
                //     # code...
                //     break;
            }
        }
        else{
            $hours = $tt->tot_hours;

            foreach ($times as $time) {
                $hours += $time->tot_hours;
            }

            //$hour = $hours + $tt->tot_hours;

            switch ($tt->category) {
                case 'Lecture':
                    if ($hours > $module->lec_hours) {
                        abort(503, 'Total Hours exceeds the module lecture hours('.$module->lec_hours.')!');
                    }
                    break;

                case 'Tutorial':
                    if ($hours > $module->tute_hours) {
                        abort(503, 'Total Hours exceeds the module tutorial hours('.$module->tute_hours.')!');
                    }
                    break;

                case 'Lab Session':
                    if ($hours > $module->lab_hours) {
                        abort(503, 'Total Hours exceeds the module lab hours('.$module->lab_hours.')!');
                    }
                    break;
                
                // default:
                //     # code...
                //     break;
            }
        }

        $tt->save();

        $log = SystemLog::logRep('Timetable', 'C', 'Creating Timetable By - '. Auth::user()->name);

        return redirect()->route('timetable.index')
                        ->with('success','Product created successfully.');  
          
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Timetable  $timetable
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $timetable= Timetable::find($id);

        $module = Module::where('id', $timetable->module)->first();

        return view('Frontend.Timetable.view', compact('timetable','module'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Timetable  $timetable
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $timetable= Timetable::find($id);
        $modules = Module::all();

        return view('Frontend.Timetable.update', compact('timetable', 'modules'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Timetable  $timetable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([  
            'ddate'=>'required',  
            'module'=>'required',  
            'category'=>'required',  
            'tot_hours'=>'required|integer|between:1,200',
        ],[
            'ddate.required'=>'Date is required!',  
            'module.required'=>'Module is required!',  
            'category.required'=>'Category is required!',  
            'tot_hours.required'=>'Total Hours is required!',
            'tot_hours.between'=>'Total Hours must be greater than 0!'
        ]);   
  
        $tt = Timetable::find($id);  
        $tt->ddate =  $request->get('ddate');  
        $tt->module = $request->get('module');  
        $tt->category = $request->get('category');  
        $tt->tot_hours = $request->get('tot_hours');
        $tt->remarks = $request->get('remarks');

        $module = Module::find($tt->module)->first();

        $times = Timetable::where('module' , $tt->module)
                        ->where('category' , $tt->category)->get();

        if($times->isEmpty()){

            switch ($tt->category) {
                case 'Lecture':
                    if ($tt->tot_hours > $module->lec_hours) {
                        abort(503, 'Total Hours('.$tt->tot_hours.') exceeds the module lecture hours('.$module->lec_hours.')!');
                    }
                    break;

                case 'Tutorial':
                    if ($tt->tot_hours > $module->tute_hours) {
                        abort(503, 'Total Hours('.$tt->tot_hours.') exceeds the module tutorial hours('.$module->tute_hours.')!');
                    }
                    break;

                case 'Lab Session':
                    if ($tt->tot_hours > $module->lab_hours) {
                        abort(503, 'Total Hours('.$tt->tot_hours.') exceeds the module lab hours('.$module->lab_hours.')!');
                    }
                    break;
                
                // default:
                //     # code...
                //     break;
            }
        }
        else{
            $hours = $tt->tot_hours;

            foreach ($times as $time) {
                $hours += $time->tot_hours;
            }

            //$hour = $hours + $tt->tot_hours;

            switch ($tt->category) {
                case 'Lecture':
                    if ($hours > $module->lec_hours) {
                        abort(503, 'Total Hours exceeds the module lecture hours('.$module->lec_hours.')!');
                    }
                    break;

                case 'Tutorial':
                    if ($hours > $module->tute_hours) {
                        abort(503, 'Total Hours exceeds the module tutorial hours('.$module->tute_hours.')!');
                    }
                    break;

                case 'Lab Session':
                    if ($hours > $module->lab_hours) {
                        abort(503, 'Total Hours exceeds the module lab hours('.$module->lab_hours.')!');
                    }
                    break;
                
                // default:
                //     # code...
                //     break;
            }
        }

        $tt->save();
        $log = SystemLog::logRep('Timetable', 'E', 'Update timetable ID - '. $tt->id . ', By - '. Auth::user()->name);

        return redirect()->route('timetable.index')
                        ->with('success','Product created successfully.');
    } 

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Timetable  $timetable
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tt=Timetable::find($id);

        $log = SystemLog::logRep('Timetable', 'D', 'Delete timetable ID - '. $id . ', By - '. Auth::user()->name);

        $tt->delete();

        $timetable = Timetable::all();
        return view('Frontend.Timetable.index', compact('timetable'));
    }
}

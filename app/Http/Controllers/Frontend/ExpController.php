<?php

namespace App\Http\Controllers\Frontend;

use App\ExpModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $working_exp = ExpModel::all();

         return view('Frontend.Biography.view', compact('working_exp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('Frontend.Biography.exp');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([  
            'start_date'=>'required', 
            // 'last_name'=>'required',  
            // 'gender'=>'required',  
            // 'qualifications'=>'required'  
        ]);  

        $ex = new ExpModel;  
        $ex->designation =  $request->get('designation');  
       // $pd->prof_img = $request->get('prof_img');  
        $ex->organization = $request->get('organization');
        $ex->years = $request->get('years');
        $ex->start_date = $request->get('start_date');
        $ex->remarks = $request->get('remarks');
        $ex->userid = 'vdv';
        $ex->save();

         return redirect()->route('working_exp.view')
                        ->with('success','Details created successfully.'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExpModel  $expModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $working_exp = ExpModel::find($id);
         return view('Frontend.Biography.view', compact('working_exp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExpModel  $expModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $working_exp = ExpModel::find($id);
         return view('Frontend.Biography.view', compact('working_exp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExpModel  $expModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([  
            'start_date'=>'required', 
            // 'last_name'=>'required',  
            // 'gender'=>'required',  
            // 'qualifications'=>'required'  
        ]);  

            $ex = ExpModel::find($id);  
            $ex->designation =  $request->get('designation');  
       // $pd->prof_img = $request->get('prof_img');  
            $ex->organization = $request->get('organization');
            $ex->years = $request->get('years');
            $ex->start_date = $request->get('start_date');
            $ex->remarks = $request->get('remarks');
            $ex->save();

            return redirect()->route('working_exp.view')
                        ->with('success','Details created successfully.'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExpModel  $expModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ex=ExpModel::find($id);  
        $ex->delete();

         $working_exp = ExpModel::all();
          return view('Frontend.Biography.view', compact('working_exp'));
    }
}

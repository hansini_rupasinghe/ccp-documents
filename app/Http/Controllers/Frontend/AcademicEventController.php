<?php

namespace App\Http\Controllers\Frontend;

use App\AcademicEvents;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AcademicEventController extends Controller
{

    public function index()
    {
        $events = AcademicEvents::all();
        return view('Frontend.AcademicEvents.events', compact('events'));

    }

    public function create()
    {
        return view('Frontend.AcademicEvents.addEvents');
    }

    public function stores(Request $request){
    	 $request->validate([  
            'ename'=>'required',  
            'edesp'=>'required',  
            'organizer'=>'required',  
            'edate'=>'required',
            'etime'=>'required'
        ]);  
  
        $event = new AcademicEvents;  
        $event->ename =  $request->get('ename');  
        $event->edesp = $request->get('edesp');  
        $event->organizer = $request->get('organizer');  
        $event->edate = $request->get('edate');
        $event->etime = $request->get('etime');
        $event->save();

        return redirect()->route('academic_events.index')
                        ->with('success','Product created successfully.');  
          
    }

    public function delete($id)
    {
        $event=AcademicEvents::find($id);  
        $event->delete();

        $event = AcademicEvents::all();
        return redirect()->route('academic_events.index')
                        ->with('success','Product created successfully.');
    }

}

?>

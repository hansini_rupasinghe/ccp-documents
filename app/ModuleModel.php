<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleModel extends Model
{
    public $timestamps = false;
    
    protected $table='modules';

    protected $fillable=[
      'id', 'mod_code', 'mod_name',	'department', 'year', 'semester', 'lec_hours', 'tute_hours', 'lab_hours', 'faculty', 'credit_hours', 'max_lec'
    ]; 
}

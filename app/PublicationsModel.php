<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicationsModel extends Model
{
     public $timestamps = false;
    
    protected $table='publications';

    protected $fillable=[
        'id', 'topic', 'pub_description', 'publication_type', 'pub_date', 'pub_version'
    ]; 
}

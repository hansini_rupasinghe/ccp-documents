<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccompModel extends Model
{
    public $timestamps = false;
    
    protected $table='accomplishments';

    protected $fillable=[
        'userid', 'title', 'desp', 
    ]; 
}

